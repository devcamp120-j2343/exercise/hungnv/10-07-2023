//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');
//Khoi tao app node.js bang express
const app = express();
//Khai bao ung dung se chay cong 8000
const port = 8000;

//Khai bao ung dung chay duoc body raw json
app.use(express.json());

//Doc duoc du lieu da duoc ma hoa
app.use(express.urlencoded());

//Khoi tao class Drink
class Drink {
    constructor(id, code, name, price, createdDate, updatedDate) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.price = price;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }
}
//Khoi tao API get drink
app.get("/drinks-class", (request, response) => {
    const drink = [
        new Drink(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021"),
        new Drink(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021"),
        new Drink(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021")
    ];
    const code = request.query.code;
    if (code === "") {
        response.json(drink);
    } else {
        const filteredDrinks = drink.filter(drink => drink.code === code);
        response.json(filteredDrinks);
    }
});
//Khoi tao API get object Drink
app.get("/drinks-object", (request, response) => {
    const drinks = [
        {
            id: 1,
            code: "TRATAC",
            name: "Trà tắc",
            price: 10000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        },
        {
            id: 2,
            code: "COCA",
            name: "Cocacola",
            price: 15000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        },
        {
            id: 3,
            code: "PEPSI",
            name: "Pepsi",
            price: 15000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        }
    ];

    const code = request.query.code;
    if (code === "") {
        response.json(drinks);
    } else {
        const filteredDrinks = drinks.filter(drink => drink.code === code);
        response.json(filteredDrinks);
    }
});
//Them API drinkId tra ve class Drink
app.get("/drinks-class/:drinkId", (request, response) => {
    const drinks = [
        new Drink(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021"),
        new Drink(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021"),
        new Drink(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021")
    ];

    const drinkId = request.params.drinkId;
    const filteredDrink = drinks.find(drink => drink.id === parseInt(drinkId));

    if (filteredDrink) {
        response.json(filteredDrink);
    } else {
        response.status(404).json({ message: "Drink not found" });
    }
});
//Them API dinkId tra ve object drink
app.get("/drinks-object/:drinkId", (request, response) => {
    const drinks = [
        {
            id: 1,
            code: "TRATAC",
            name: "Trà tắc",
            price: 10000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        },
        {
            id: 2,
            code: "COCA",
            name: "Cocacola",
            price: 15000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        },
        {
            id: 3,
            code: "PEPSI",
            name: "Pepsi",
            price: 15000,
            createdDate: "14/5/2021",
            updatedDate: "14/5/2021"
        }
    ];

    const drinkId = request.params.drinkId;
    const filteredDrink = drinks.find(drink => drink.id === parseInt(drinkId));

    if (filteredDrink) {
        response.json(filteredDrink);
    } else {
        response.status(404).json({ message: "Drink not found" });
    }
});



//Chay ung dung tren cong port:8000
app.listen(port, () => {
    console.log("Ung dung chay tren cong : ", port)
});