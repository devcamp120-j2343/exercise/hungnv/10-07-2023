//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');

//Khoi tao app node.js bang express
const app = express();

//Khai bao ung dung se chay cong 8000
const port = 8000;

//Khai bao ung dung chay duoc body raw json
app.use(express.json());

//Doc duoc du lieu da duoc ma hoa
app.use(express.urlencoded());

//Khai bao API tra ve chuoi
app.get("/", (request, response) => {
    //Viet code xu ly trong nay
    var today = new Date();
    var resultString = `Xin chao , hom nay la ngay ${today.getDate()} thang ${today.getMonth() + 1} nam ${today.getFullYear()}`;
    response.json({
        result: resultString
    });
})
//Tao GET API 
app.get("/get-method", (request, response) => {
    response.json({
        method: "GET"
    });
});
//Tao POST API 
app.post("/post-method", (request, response) => {
    response.json({
        method: "POST"
    });
});
//Tao PUT API 
app.put("/put-method", (request, response) => {
    response.json({
        method: "PUT"
    });
});
//Tao DELETE API 
app.delete("/delete-method", (request, response) => {
    response.json({
        method: "DELETE"
    });
});
//Tao API lay request params
app.get("/get-params/:param1/:param2", (request, response) => {
    var param1 = request.params.param1;
    var param2 = request.params.param2;
    //Thao tac du lieu tu param1
    response.json({
        param1: param1,
        param2: param2
    });
});
//Tao API lay request query
app.get("/get-query", (request, response) => {
    var query = request.query;
    //Validate..

    response.json({
        query: query
    });
});

//Tao API lay request body raw json
app.post("/get-body-raw", (request, response) => {
    var body = request.body;
    //Validate..
    response.json({
        body: body
    });
});

//Chay ung dung tren cong port:8000
app.listen(port, () => {
    console.log("Ung dung chay tren cong : ", port)
});